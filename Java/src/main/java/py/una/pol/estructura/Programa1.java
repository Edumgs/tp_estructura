package py.una.pol.estructura;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Programa1 {

	private HashSet<String> links;

	public Programa1() {
		links = new HashSet<String>();
	}

	public void getPageLinks(String URL) {
		if (!links.contains(URL)) {
			try {
				Document document = Jsoup.connect(URL).get();
				Elements linksOnPage = document.select("a[href]");
			
				List<String> links = new ArrayList<String>();
				for (Element element : linksOnPage) {
					for (String item1 : element.attr("abs:href").split("/")) {
						if (item1.compareTo("") != 0 && item1.compareTo("#") != 0 && !Pattern.compile("^www.").matcher(item1).find() && !Pattern.compile("^index.").matcher(item1).find() && !Pattern.compile("^http").matcher(item1).find() && !Pattern.compile("^[a-zA-Z]*.(com|edu|py|una)").matcher(item1).find()) {
							for (String item2 : item1.split("-")) {	
								for (String item3 : item2.split("\\.")) {
									if (Pattern.compile("[a-zA-Z]+").matcher(item3).find()) {
										links.add(item3);										
									}
								}
							}
						}
					}
				}
				Map<String, Integer> palabras = new HashMap<String, Integer>();
				for (String link : links) {
					if (palabras.get(link) == null) {
						palabras.put(link, 1);
					} else {
						Integer aux = palabras.get(link);
						palabras.remove(link);
						palabras.put(link, aux + 1);
					}
				}
				guardarLista("cuentaPalabra.txt", palabras);
				
			} catch (IOException e) {
				System.err.println("For '" + URL + "': " + e.getMessage());
			}
		}
	}
	
	public static void guardarLista(String nombre, Map<String, Integer> palabras) {
		FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("/home/edu/MEGAsync/Estructura/Java/src/main/java/py/una/pol/estructura/" + nombre);
            pw = new PrintWriter(fichero);

            Iterator<String> it = palabras.keySet().iterator();
			while (it.hasNext()) {
			  String key = it.next();
			  pw.println(palabras.get(key) + ";" + key);
			}

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}

	public static void main(String[] args) {
		new Programa1().getPageLinks("http://www.educa.una.py/sitio");
	}
}
