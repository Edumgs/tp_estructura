package py.una.pol.estructura;

import java.io.*;
import java.util.*;

public class Programa2 {

	public static void ordenamietoPorMezcla(List<List<String>> lista) {
		if (lista.size() > 1) {
			int mitad = (int) (lista.size() / 2);
			List<List<String>> listaIzq = new ArrayList<List<String>>();
			for (int i = 0; i < mitad; i++)
				listaIzq.add(lista.get(i));
			List<List<String>> listaDer = new ArrayList<List<String>>();
			for (int i = mitad; i < lista.size(); i++)
				listaDer.add(lista.get(i));

			ordenamietoPorMezcla(listaIzq);
			ordenamietoPorMezcla(listaDer);
			int i = 0;
			int j = 0;
			int k = 0;
			while (i < listaIzq.size() && j < listaDer.size()) {
				if (Integer.parseInt(listaIzq.get(i).get(0)) < Integer.parseInt(listaDer.get(j).get(0))) {
					lista.set(k, listaIzq.get(i));
					i++;
				} else {
					lista.set(k, listaDer.get(j));
					j++;
				}
				k++;
			}
			while (i < listaIzq.size()) {
				lista.set(k, listaIzq.get(i));
				i++;
				k++;
			}
			while (j < listaDer.size()) {
				lista.set(k, listaDer.get(j));
				j++;
				k++;
			}
		}
	}

	public static void ordenamietoPorBurbuja(List<List<String>> lista) {
		for (int i = 1; i < lista.size() - 1; i++) {
			for (int j = 0; j < lista.size() - i; j++) {
				if (lista.get(j).get(1).compareTo(lista.get(j + 1).get(1)) > 0) {
					List<String> aux = lista.get(j);
					lista.set(j, lista.get(j + 1));
					lista.set(j + 1, aux);
				}
			}
		}
	}

	public static void guardarLista(String nombre, List<List<String>> lista) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter("/home/edu/MEGAsync/Estructura/Java/src/main/java/py/una/pol/estructura/" + nombre);
			pw = new PrintWriter(fichero);

			for (int i = 0; i < lista.size(); i++)
				pw.println(lista.get(i).get(0) + ";" + lista.get(i).get(1));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public static void main(String[] arg) {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			archivo = new File("/home/edu/MEGAsync/Estructura/Java/src/main/java/py/una/pol/estructura/cuentaPalabra.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			List<List<String>> lista = new ArrayList<List<String>>();
			String linea;
			while ((linea = br.readLine()) != null) {
				List<String> aux = new ArrayList<String>();
				aux.add(linea.split(";")[0]);
				aux.add(linea.split(";")[1]);
				lista.add(aux);
			}
			ordenamietoPorMezcla(lista);
			guardarLista("ordenadoPorCuenta.txt", lista);
			ordenamietoPorBurbuja(lista);
			guardarLista("ordenadoPorPalabra.txt", lista);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

}