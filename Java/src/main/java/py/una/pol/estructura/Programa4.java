package py.una.pol.estructura;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Programa4 {
	public static int busquedaBinaria(List<List<String>> lista, String valor, int izq, int der) {
		if (izq > der) {
			return -1;
		}
		int medio = (int)((izq + der) / 2);
		
		if (lista.get(medio).get(1).compareTo(valor) == 0) {
			return medio;
		}
		if (valor.compareTo(lista.get(medio).get(1)) < 0) {
			return busquedaBinaria(lista, valor, izq, medio - 1);
		} else {
			return busquedaBinaria(lista, valor, medio + 1, der);
		}
	}
	
	public static void main(String[] arg) {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			archivo = new File("/home/edu/MEGAsync/Estructura/Java/src/main/java/py/una/pol/estructura/ordenadoPorPalabra.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			List<List<String>> lista = new ArrayList<List<String>>();
			String linea;
			while ((linea = br.readLine()) != null) {
				List<String> aux = new ArrayList<String>();
				aux.add(linea.split(";")[0]);
				aux.add(linea.split(";")[1]);
				lista.add(aux);
			}
			int posicion = busquedaBinaria(lista, "sitio", 0, lista.size() - 1);
			if (posicion == -1) {
				System.out.println("No hay valor encontrado");
			} else {
				System.out.println("Esta en la posicion: " + posicion);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

}
