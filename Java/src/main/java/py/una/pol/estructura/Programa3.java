package py.una.pol.estructura;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Programa3 {

	public static void main(String[] arg) {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			archivo = new File("/home/edu/MEGAsync/Estructura/Java/src/main/java/py/una/pol/estructura/ordenadoPorCuenta.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			List<List<String>> lista = new ArrayList<List<String>>();
			String linea;
			while ((linea = br.readLine()) != null) {
				List<String> aux = new ArrayList<String>();
				aux.add(linea.split(";")[0]);
				aux.add(linea.split(";")[1]);
				lista.add(aux);
			}

			System.out.println("Mayor numero: " + lista.get(lista.size() - 1).get(0));
			int suma = 0;
			for (List<String> list : lista) {
				suma += Integer.parseInt(list.get(0));
			}
			System.out.println("Promedio: " + (suma / lista.size()));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

}
