const fs = require('fs');

fs.readFile('ordenadoPorPalabra.txt', 'utf8', (err, data) => {
  if (err) {
    return console.log(err);
  }
  let lista = [];

  for (const item of data.split('\n')) {
    if (item != '') {
      let aux = [Number(item.split(';')[0]), item.split(';')[1]];
      lista.push(aux);
    }
  }

  posicion = busquedaBinaria(lista, 'sitio', 0, (lista.length - 1));
  if (posicion == -1) {
    console.log('No hay valor');
  } else {
    console.log('Esta en la posicion: ', posicion);
  }
});

function busquedaBinaria(lista, valor, izq, der) {
  if (izq > der){
    return -1;
  }
  medio = parseInt((izq + der) / 2);

  if (lista[medio][1] == valor) {
    return medio;
  }
  if (valor < lista[medio][1]) {
    return busquedaBinaria(lista, valor, izq, medio - 1);
  } else {
    return busquedaBinaria(lista, valor, medio + 1, der);
  }
}