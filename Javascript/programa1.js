const Crawler = require('js-crawler');
const fs = require('fs');

function main() {
  guardarCuentaPalabra();
}

function guardarPalabras(url) {
  const stream = fs.createWriteStream("palabra.txt");
  new Crawler().configure({depth: 3})
    .crawl(url, function onSuccess (page) {
      for (const item1 of (page.url).split('/')) {    
        if (item1 != '' && item1.match(/(index.)+/) == null && item1.match(/(www.)+/) == null && item1.match(/(http)+/) == null && item1.match(/[a-zA-Z]*.(com|edu|py|una)/) == null) {
          for (const item2 of item1.split('-')) {
            for (const item3 of item2.split('.')) {
              if (item3.match(/[0-9]+/) == null) {
                console.log(item3);
                stream.write(item3+'\n');
              }
            }
          }
        }
      }
    });
}

function guardarCuentaPalabra() {
  fs.readFile('palabra.txt', 'utf8', function(err, data) {
    if (err) {
      return console.log(err);
    }

    let lista = new Array();
    for (const item of data.split('\n')) {
      if (item != '') {
        if (lista[item] == null) {
          lista[item] = 1;
        } else {
          lista[item] += 1;
        }        
      }
    }
    const stream = fs.createWriteStream('cuentaPalabra.txt');
    for (const key in lista) {
      stream.write(lista[key].toString() + ";" + key + "\n");
    }
    stream.close();
  });
}

main();
