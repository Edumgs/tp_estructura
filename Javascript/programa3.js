const fs = require('fs');

fs.readFile('ordenadoPorCuenta.txt', 'utf8', (err, data) => {
  if (err) {
    return console.log(err);
  }
  let lista = [];

  for (const item of data.split('\n')) {
    if (item != '') {
      let aux = [Number(item.split(';')[0]), item.split(';')[1]];
      lista.push(aux);
    }
  }
  console.log("Mayor numero: ", lista[lista.length-1][0]);
  let suma = 0;
  for (let i = 0; i < lista.length; i++) {
    suma += lista[i][0]; 
  }
  console.log("Promedio: ", (suma/lista.length));
});