const fs = require('fs');

fs.readFile('cuentaPalabra.txt', 'utf8', (err, data) => {
  if (err) {
    return console.log(err);
  }
  let lista = [];

  for (const item of data.split('\n')) {
    if (item != '') {
      let aux = [Number(item.split(';')[0]), item.split(';')[1]];
      lista.push(aux);
    }
  }
  ordenamientoPorBurbuja(lista);
  guardarLista("ordenadoPorPalabra.txt", lista);
  ordenamientoPorMezcla(lista);
  guardarLista("ordenadoPorCuenta.txt", lista);
});

function ordenamientoPorMezcla(lista) {
  if (lista.length > 1) {
    const mitad = parseInt(lista.length / 2) | 0;
    const listaIzq = lista.slice(0, mitad);
    const listaDer = lista.slice(mitad);

    ordenamientoPorMezcla(listaIzq);
    ordenamientoPorMezcla(listaDer);

    i=0;j=0;k=0;
    while (i < listaIzq.length && j < listaDer.length) {      
      if (listaIzq[i][0] < listaDer[j][0]) {
        lista[k] = listaIzq[i];
        i=i+1;
      } else {
        lista[k] = listaDer[j];
        j=j+1;
      }
      k=k+1;
    }

    while (i < listaIzq.length) {
      lista[k] = listaIzq[i];
      i=i+1;
      k=k+1;
    }

    while (j < listaDer.length) {
      lista[k] = listaDer[j];
      j=j+1;
      k=k+1;
    }
  }
}

function ordenamientoPorBurbuja(lista) {
  for (let i = 1; i < lista.length - 1; i++) {
    for (let j = 0; j < lista.length - i; j++) {
      if (lista[j][1] > lista[j+1][1]) {
        aux = lista[j+1];
        lista[j+1] = lista[j];
        lista[j] = aux;
      }
    }
  }
}

function guardarLista(nombre, lista) {
  const stream = fs.createWriteStream(nombre);
  lista.forEach(element => {
    stream.write(element[0].toString() + ";" + element[1] + "\n");
  });
  stream.end();
}