def main():
  archivo = open("cuentaPalabra.txt", "r")

  lista = []
  for linea in archivo:
    lista.append([linea.split('\n')[0].split(';')[1], int(linea.split('\n')[0].split(';')[0])])

  ordenamientoPorBurbuja(lista)
  guardarLista("ordenadoPorPalabra.txt", lista)
  ordenamientoPorMezcla(lista)
  guardarLista("ordenadoPorCuenta.txt", lista)
  

def ordenamientoPorMezcla(lista):
  if len(lista) > 1:
    mitad = len(lista) // 2
    listaIzq = lista[:mitad]
    listaDer = lista[mitad:]

    ordenamientoPorMezcla(listaIzq)
    ordenamientoPorMezcla(listaDer)

    i, j, k = 0, 0, 0
    while i < len(listaIzq) and j < len(listaDer):
      if listaIzq[i][1] < listaDer[j][1]:
        lista[k] = listaIzq[i]
        i=i+1
      else:
        lista[k] = listaDer[j]
        j=j+1
      k=k+1

    while i < len(listaIzq):
      lista[k] = listaIzq[i]
      i=i+1
      k=k+1

    while j < len(listaDer):
      lista[k] = listaDer[j]
      j=j+1
      k=k+1


def ordenamientoPorBurbuja(lista):
  for i in range(1, len(lista)-1):
    for j in range(0, len(lista)-i):
      if lista[j][0] > lista[j+1][0]:
        aux = lista[j]
        lista[j] = lista[j+1]
        lista[j+1] = aux


def guardarLista(nombre, lista):
  archivo = open(nombre, "w")
  for valor in lista:
    archivo.write(str(valor[1]) + ';' + valor[0] + '\n')
  archivo.close()


main()