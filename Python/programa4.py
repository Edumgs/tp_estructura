def main():
  archivo = open("ordenadoPorPalabra.txt", "r")

  lista = []
  for linea in archivo:
    lista.append([linea.split('\n')[0].split(';')[1], int(linea.split('\n')[0].split(';')[0])])
  
  posicion = busquedaBinaria(lista, 'sitio', 0, len(lista) - 1)
  if posicion == -1:
    print('No hay valor')
  else:
    print('Esta en la posicion: ', posicion)


def busquedaBinaria(lista, valor, izq, der):
    if izq > der:
      return -1
    medio = (izq + der) // 2

    if lista[medio][0] == valor:
        return medio
    if valor < lista[medio][0]:
        return busquedaBinaria(lista, valor, izq, medio - 1)
    else:
        return busquedaBinaria(lista, valor, medio + 1, der)


main()