from bs4 import BeautifulSoup
import requests
import re

url = 'http://www.educa.una.py/sitio/'
html = BeautifulSoup(requests.get(url).text, "html.parser")

links = []
for link in html.find_all('a'):
  if link.get('href') != '#' and link.get('href') != None:
    links.append(link.get('href'))

temp = []
for item1 in links:
  for item2 in item1.split('/'):
    if item2 != '' and not re.match('(index.)+', item2) and not re.match('(www.)+', item2) and not re.match('(http)+', item2) and not re.match('[a-zA-Z]*.(com|edu|py|una)', item2):
      for item3 in item2.split('-'):
        for item4 in item3.split('.'):
          if re.match('[a-zA-Z]+', item4):
            temp.append(item4)

palabras = {}
for item in temp:
  if palabras.get(item) == None:
    palabras[item] = 1
  else:
    palabras[item] += 1

archivo = open("cuentaPalabra.txt", "w")
for clave, valor in palabras.items():
  archivo.write(str(valor) + ';' + clave + '\n')
archivo.close()