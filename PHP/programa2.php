<?php
function main() {
  $archivo = fopen("cuentaPalabra.txt", "r");
  $temp = "";
  while(!feof($archivo)){
    $temp = $temp.fread($archivo, 4092);
  }
  fclose($archivo);
  
  $lista = array();
  foreach (explode("\n", $temp) as $key => $value) {
    if ($value != "") {
      $aux = [explode(";", $value)[0], explode(";", $value)[1]];
      array_push($lista, $aux);
    }
  }

  ordenamientoPorBurbuja($lista);
  guardarLista("ordenadoPorPalabra.txt", $lista);
  ordenamientoPorMezcla($lista);
  guardarLista("ordenadoPorCuenta.txt", $lista);
}

function ordenamientoPorMezcla(&$lista) {
  if (count($lista) > 1) {
    $medio = (int) count($lista) / 2;
    $listaIzq = array_slice($lista, 0, $medio);
    $listaDer = array_slice($lista, $medio);

    ordenamientoPorMezcla($listaIzq);
    ordenamientoPorMezcla($listaDer);
    
    $i=0;$j=0;$k=0;
    while ($i < count($listaIzq) and $j < count($listaDer)) {
      if ($listaIzq[$i][0] < $listaDer[$j][0]) {
        $lista[$k] = $listaIzq[$i];
        $i+=1;
      } else {
        $lista[$k] = $listaDer[$j];
        $j+=1;
      }
      $k+=1;
    }
    while ($i < count($listaIzq)) {
      $lista[$k] = $listaIzq[$i];
      $i+=1;
      $k+=1;
    }
    while ($j < count($listaDer)) {
      $lista[$k] = $listaDer[$j];
      $j+=1;
      $k+=1;
    }
  }
}

function ordenamientoPorBurbuja(&$lista) {
  for ($i=1; $i < count($lista) - 1; $i++) { 
    for ($j=0; $j < count($lista) - $i; $j++) { 
      if ($lista[$j][1] > $lista[$j+1][1]) {
        $aux = $lista[$j];
        $lista[$j] = $lista[$j+1];
        $lista[$j+1] = $aux;
      }
    }
  }
}

function guardarLista($nombre, $lista) {
  $archivo = fopen($nombre, "w");
  for ($i=0; $i < count($lista); $i++) { 
    fwrite($archivo, (string) $lista[$i][0].";".$lista[$i][1]."\n");
  }
}

main();

?>