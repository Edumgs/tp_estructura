<?php
  $url = file_get_contents('http://www.educa.una.py/sitio');
  $dom = new DOMDocument();
  @$dom->loadHTML($url);
  $xpath = new DOMXPath($dom);
  $hrefs = $xpath->evaluate("/html/body//a");

  $links = array();
  for ($i = 0; $i < $hrefs->length; $i++) {
    $href = $hrefs->item($i);
    if ($href->getAttribute('href') != '#' and $href->getAttribute('href') != '') {
      array_push($links, $href->getAttribute('href'));
    }
  }

  $temp = array();
  foreach ($links as $key1 => $item1) {
    foreach (explode("/", $item1) as $key2 => $item2) {
      if ($item2 != "" and !preg_match("/(index.)+/", $item2) and !preg_match("/(www.)+/", $item2) and !preg_match("/(http)+/", $item2) and !preg_match("/[a-zA-Z]*.(com|edu|py|una)/", $item2)) {
        foreach (explode("-", $item2) as $key3 => $item3) {
          foreach (explode(".", $item3) as $key4 => $item4) {
            if (preg_match("/[a-zA-Z]+/", $item4)) {
              array_push($temp, $item4);
            }
          }
        }
      }
    }
  }

  $palabras = array();
  foreach ($temp as $key => $value) {
    if (array_key_exists($value, $palabras)) {
      $palabras[$value] += 1;
    } else {
      $palabras[$value] = 1;
    }
  }

  $archivo = fopen("cuentaPalabra.txt", "w");
  foreach ($palabras as $key => $value) {
    fwrite($archivo, (string) $value.";".$key."\n");
  }
  fclose($archivo);
  
?>