<?php
function main() {
  $archivo = fopen("ordenadoPorPalabra.txt", "r");
  $temp = "";
  while(!feof($archivo)){
    $temp = $temp.fread($archivo, 4092);
  }
  fclose($archivo);
  
  $lista = array();
  foreach (explode("\n", $temp) as $key => $value) {
    if ($value != "") {
      $aux = [explode(";", $value)[0], explode(";", $value)[1]];
      array_push($lista, $aux);
    }
  }

  $posicion = busquedaBinaria($lista, "hola", 0, count($lista) - 1);
  if($posicion == -1) {
    echo "No hay valor\n";
  } else {
    echo "Esta en la posicion: ".$posicion."\n";
  }
}

function busquedaBinaria($lista, $valor, $izq, $der) {
  if ($izq > $der) {
    echo "Entro";
    return -1;
  }
  $medio = (int) (($izq + $der) / 2);
  if (strcmp($valor, $lista[$medio][1]) == 0) {
    return $medio;
  }
  if (strcmp($valor, $lista[$medio][1]) < 0) {
    return busquedaBinaria($lista, $valor, $izq, $medio);
  } else {
    return busquedaBinaria($lista, $valor, $medio, $der);
  }
}

main();
?>